## Welcome to CharcBox Mobile App!

**:warning: Information**: For now we are mocking CharcBox API using [JsonServer](https://github.com/typicode/json-server).

#### TODO
- [x] AutoDiscover IP of the CharcBox(s) on the network
- [ ] Charbox Initial Setup (change Network settings etc.)
- [x] Add value checking w/ threshold for Settings change
- [ ] Add visual health indicator for each Settings
- [x] Add settings update error message / visual feedback

#### Install Commands

First install XCode from the AppStore and then:
```
brew install node
brew install yarn
brew install watchman
npm install -g react-native-cli
```

#### Usage
```
yarn install
json-server api-mock/db.json --watch -p 8080 -d 500 //start local bonjour server for testing purpose
react-native start-ios
```

#### Troubleshooting

- If you encounter an error at the install the first time you run React and it talks about **XCode Path Error**, read this:
http://stackoverflow.com/questions/39778607/error-running-react-native-app-from-terminal-ios

#### Screenshot

![](http://i.imgur.com/hG5yRmh.png)
