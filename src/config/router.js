import React from 'react'
import {TabNavigator, StackNavigator} from 'react-navigation'
import { Icon } from 'react-native-elements'

import LaunchContainer from '../screens/launch/launchContainer'
import DeviceContainer from '../screens/device/deviceContainer'
import BonjourContainer from '../screens/bonjour/bonjourContainer'
import Bonjour from '../screens/bonjour/bonjour'
import About from '../screens/about/about'

export const BonjourStack = StackNavigator({
  BonjourContainer: {
    screen: BonjourContainer,
    navigationOptions: {
      title: 'Network',
      header: {
        backTitle:null,
        tintColor: '#ffffff',
        titleStyle: {
          fontSize: 22
        },
        style: {
          backgroundColor:'#e06666ff'
        }
      }
    },
  },

  Bonjour: {
    screen: Bonjour,
    navigationOptions: {
      title: ({ state }) => `${state.params.name}`,
      header: {
        tintColor: '#ffffff',
        style: {
          backgroundColor:'#e06666ff'
        }
      }
    },
  },
})

export const Tabs = TabNavigator({
  LaunchContainer: {
    screen: LaunchContainer,
    navigationOptions: {
      tabBar: {
        label: 'Device',
        icon: ({ tintColor }) => <Icon name="power" size={35} color={tintColor} />
      },
    },
  },
  Bonjour: {
    screen: BonjourStack,
    navigationOptions: {
      tabBar: {
        label: 'Network',
        icon: ({ tintColor }) => <Icon name="wifi" size={35} color={tintColor} />
      },
    },
  },
  About: {
    screen: About,
    navigationOptions: {
      tabBar: {
        label: 'About',
        icon: ({ tintColor }) => <Icon name="info" size={35} color={tintColor} />
      },
    },
  },
},
{
  tabBarOptions: {
    activeTintColor: '#e06666ff',
  }
})
