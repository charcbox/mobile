var EventEmitter = require('events').EventEmitter

var emitter = new EventEmitter()

var devices = []

module.exports = {
  getDevices: function(){
    return [...new Set(devices)] //deduplicate
  },

  subscribe: function(callback){
    emitter.addListener('update', callback)
  },

  unsubscribe: function(callback){
    emitter.removeListener('update', callback)
  },

  setDevices: function(ips){
    devices = ips
    emitter.emit('update')
  }
}
