import React, { Component } from 'react';
import {
  View,
  Text,
  ActivityIndicator
} from 'react-native'

import MainContainer from '../partials/mainContainer'
import Devices from './devices'

export default class DeviceContainer extends Component {
  constructor(props){
    super(props)
  }

  render(){
    let data = null

    if (this.props.isOnWifi()) {
      if (this.props.devices().length == 0) {
        data = <MainContainer header="big" title="CharcBox">
          <ActivityIndicator/>
        </MainContainer>
      } else {
        data = <Devices devices={this.props.devices}/>
      }
    } else {
     data = <MainContainer header="big" title="CharcBox">
       <Text>Please connect to your home Wifi to access your CharcBox</Text>
     </MainContainer>
    }

    return(
      <View style={{flex:1}}>
        {data}
      </View>
    )
  }
}
