import React, { Component } from 'react';
import { TouchableHighlight, StyleSheet, ScrollView, Slider, View, Text } from 'react-native';
import { Button } from 'react-native-elements';

var styles = StyleSheet.create({
  text: {
    fontSize: 14,
    textAlign: 'center',
    fontWeight: '500',
    margin: 10,
    fontStyle: 'italic',
  },
  view: {
    flex: 1,
    marginBottom: 20
  },
  title: {
    flex: 1,
    fontSize:20,
    lineHeight: 50
  },
  value1: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#ffffff'
  },
  value2: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: "#cccccc"
  },
  valueText: {
    fontSize: 22,
    fontWeight: 'bold'
  },
  touchable: {
    flex: 1
  },
  viewUpdate: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: "#cccccc",
    backgroundColor:'#fce5cdff'
  },
  textUpdateValue: {
    fontSize: 16,
    textAlign:'center'
  },
  textUpdateTouch: {
    fontSize: 11,
    textAlign: 'center',
    fontStyle: 'italic'
  },
  viewSlider: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: "#cccccc",
    backgroundColor: '#ffffff'
  },
  viewSlider2: {
    flex:4,
    alignItems: 'center'
  },
  slider: {
    width: '80%'
  },
  textSlider: {
    flex: 1,
    alignItems: 'center'
  }
});

export default class Setting extends Component {
  constructor(props) {
    super(props)
    this.state = { show: false }
  }

  updateShow(value) {
    this.setState({show: value})
  }

  _onPressButton() {
    this.updateShow(true)
  }

  onSlidingComplete(value) {
    this.props.putDeviceData(this.props.input, value)
    this.updateShow(false)
  }

  onValueChange(value) {
    this.props.updateSetting(this.props.input, value)
  }

  render() {
    return(
      <View style={styles.view}>
        <Text style={styles.title}>
          {this.props.title}
        </Text>

        { !this.state.show &&
          <View style={styles.value1}>
            <View style={styles.value2}>
                <Text style={styles.valueText}>
                  {this.props.live}{this.props.sign}
                </Text>
              </View>

              <TouchableHighlight style={styles.touchable} onPress={() => this._onPressButton()} underlayColor="red">
                <View style={styles.viewUpdate}>
                  <Text style={styles.textUpdateValue}>
                    Set to {this.props.setting} {this.props.sign}
                  </Text>
                  <Text style={styles.textUpdateTouch}>
                    Touch to change
                  </Text>
                </View>
              </TouchableHighlight>
          </View>
        }

        { this.state.show &&
            <View style={styles.viewSlider}>
              <View style={styles.viewSlider2}>
                <Slider
                  {...this.props}
                  style={styles.slider}
                  value={this.props.setting}
                  onSlidingComplete={(value) => this.onSlidingComplete(value)}
                  onValueChange={(value) => this.onValueChange(value)}
                />
              </View>
              <View style={styles.textSlider}>
                <Text style={styles.text}>{this.props.setting}{this.props.sign}</Text>
              </View>
            </View>
        }
      </View>
    )
  }
}
