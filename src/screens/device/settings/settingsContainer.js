import React, { Component } from 'react';
import { TouchableHighlight, StyleSheet, View, Text, ActivityIndicator } from 'react-native';
import { Icon } from 'react-native-elements'

import Setting from './setting'

export default class SettingsContainer extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    let data = null

    if (this.props.apiError != null){
      data =  <View style={{flex:1}}>
        <Text style={{fontWeight:'bold', color:'red', textAlign:'center', marginTop:100}}>
          <Icon name='report-problem' color='red' containerStyle={{width:40, height:20}}/>
          API Error: {this.props.apiError.message}
        </Text>
      </View>
    }

    if (this.props.isFetchingData){
      data = <ActivityIndicator/>
    }

    if (!this.props.isFetchingData && this.props.apiError == null){
      data = <View style={{flex:1}}>
        <Setting
          {...this.props}
          title={'Temperature'}
          input={'temperature'}
          setting={this.props.apiData.settings.temperature}
          live={this.props.apiData.live.temperature}
          putDeviceData={this.props.putDeviceData}
          updateSetting={this.props.updateSetting}
          minimumValue={10}
          maximumValue={20}
          minimumTrackTintColor={'blue'}
          maximumTrackTintColor={'red'}
          step={1}
          sign={'ºC'}
        />
        <Setting
          {...this.props}
          title={'Humidity'}
          input={'humidity'}
          setting={this.props.apiData.settings.humidity}
          live={this.props.apiData.live.humidity}
          putDeviceData={this.props.putDeviceData}
          updateSetting={this.props.updateSetting}
          minimumValue={60}
          maximumValue={80}
          step={1}
          sign={'%'}
        />
        <Setting
          {...this.props}
          title={'Weight'}
          input={'weight'}
          setting={this.props.apiData.settings.weight}
          live={this.props.apiData.live.weight}
          putDeviceData={this.props.putDeviceData}
          updateSetting={this.props.updateSetting}
          minimumValue={300}
          maximumValue={3000}
          step={50}
          sign={'g'}
        />
      </View>
    }

    return (
      <View style={{flex:1}}>
        {data}
      </View>
    )
  }
}
