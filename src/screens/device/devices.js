import React, { Component } from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  PickerIOS
} from 'react-native'

import Settings from './settings/settingsContainer'
import MainContainer from '../partials/mainContainer'
import SettingsContainer from './settings/settingsContainer'

export default class Devices extends Component {
  constructor(props){
    super(props)
    this.state = {
      activeDevice: this.props.devices()[0],
      isFetchingData: false,
      apiError: null,
      apiData: {
        settings: {
          temperature: 0,
          humidity: 0,
          weight: 0
        },
        live: {
          temperature: 0,
          humidity: 0,
          weight: 0
        }
      }
    }
    this.whichDeviceIP = this.whichDeviceIP.bind(this)
    this.getDeviceData = this.getDeviceData.bind(this)
    this.putDeviceData = this.putDeviceData.bind(this)
    this.updateSetting = this.updateSetting.bind(this)
    this.charboxApiUrl = this.charboxApiUrl.bind(this)
  }

  componentDidMount(){
    this.getDeviceData()
    this._mounted = true;
  }

  componentWillUnmount() {
    this._mounted = false;
  }

  charboxApiUrl() {
    return('http://' + this.state.activeDevice + ':8080')
  }

  async getDeviceData() {
    this.setState({isFetchingData: true, apiError: null})
    try {
      let responseStatus = await fetch(this.charboxApiUrl() + "/status")
      let responseStatusJson = await responseStatus.json()
      this._mounted && this.setState({isFetchingData: false, apiData: responseStatusJson})
    } catch(error) {
      console.debug("get data error")
      this.setState({isFetchingData: false, apiError: error})
    }
  }

  async putDeviceData(setting, value) {
    try {
      this.setState({apiError: null})

      let body = JSON.stringify({ [setting]: value })
      let response = await fetch(this.charboxApiUrl() + "/settings", {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: body
      })
      let responseJson = await response.json();
    } catch(error) {
      console.debug("post data error")
      this.setState({apiError: error})
    }
  }

  updateSetting(setting, value) {
    let values = Object.assign({}, this.state.apiData.settings, {[setting]: value})
    this.setState({apiData: {settings: values, live: this.state.apiData.live}})
  }

  whichDeviceIP() {
    return(this.state.activeDevice)
  }

  updatePicker(value) {
    this.setState({ activeDevice: value }, () => (
      this.setState({ apiData: this.getDeviceData() })
    ))
  }

  render(){
    let data = null
      if (this.props.devices){
        if (this.props.devices().length == 1){
          data = <SettingsContainer
            whichDeviceIP={this.state.activeDevice}
            apiData={this.state.apiData}
            putDeviceData={this.putDeviceData}
            isFetchingData={this.state.isFetchingData}
            apiError={this.state.apiError}
            updateSetting={this.updateSetting}/>
        }else{
          data =
            <View style={{flex:1}}>
              <Text style={{textAlign:'center', fontWeight:'bold'}}>Select you Charcbox below:</Text>
              <PickerIOS
                selectedValue={this.state.activeDevice}
                onValueChange={(device) => this.updatePicker(device)}
                itemStyle={{margin: 0, padding: 0, height:70}}
                >
                {this.props.devices().map((device, iterator) => (
                  <PickerIOS.Item key={iterator} label={device} value={device} />
                ))}
              </PickerIOS>
              <SettingsContainer
                whichDeviceIP={this.state.activeDevice}
                apiData={this.state.apiData}
                putDeviceData={this.putDeviceData}
                isFetchingData={this.state.isFetchingData}
                apiError={this.state.apiError}
                updateSetting={this.updateSetting}/>
            </View>
        }
    }

    return(
      <MainContainer header="big" title="CharcBox">
       {data}
      </MainContainer>
    )
  }
}
