import React, { Component } from 'react';
import { View, Text, ScrollView} from 'react-native';
import { List, ListItem} from 'react-native-elements';

import MainContainer from '../partials/mainContainer';

import Zeroconf from 'react-native-zeroconf'

const CharcBoxHost = "Freebox-Server.local."
const zeroconf = new Zeroconf()

export default class BonjourContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      services: {}
    }
  }

  componentWillMount() {
    zeroconf.scan()
    zeroconf.on('update', this.updateServices.bind(this))
    zeroconf.on('error', this.errorServices.bind(this))
  }

  //Needed if not will crash the App
  errorServices(){
    console.debug("ZeroConf Error Service")
  }

  updateServices() {
    let services = zeroconf.getServices()

    this.setState({
      services: services,
      address: this.setCharcBoxAddress(services)
    })
  }

  componentWillUnmount() {
    zeroconf.stop()
  }

  setCharcBoxAddress(services) {
    const serviceKey = Object.keys(services).find((key) => (
      services[key].host === CharcBoxHost &&
        services[key].addresses !== undefined //meaning not yet resolved
      )
    )

    if (serviceKey) {
      return services[serviceKey].addresses[0]
    }
  }

  getCharcBoxAddress() {
    return this.state.address
  }

  getServices() {
    let services = this.state.services

    return Object.values(services)
  }

  onLearnMore(service) {
    this.props.navigation.navigate('Bonjour', { ...service });
  }

  render() {
    return (
      <MainContainer>
        <ScrollView>
          <Text style={{fontSize: 16, textAlign:'center' }}>
            CharcBox IP is: { this.getCharcBoxAddress() }
          </Text>

          <List>
            {this.getServices().map((service, index) => (
              <ListItem
                key={index}
                title={service.name}
                subtitle={service.host}
                onPress={() => this.onLearnMore(service)}
              />
            ))}
          </List>
        </ScrollView>
      </MainContainer>
    )
  }
}
