import React, { Component } from 'react';
import { View, Text, ScrollView} from 'react-native';

import MainContainer from '../partials/mainContainer';

export default class Bonjour extends Component{
  constructor(props){
    super(props)
  }

  getAddress(addresses) {
    if(addresses != undefined) {
      return addresses[0]
    }
  }

  render(){
    const { name, host, port, addresses } = this.props.navigation.state.params;

    return(
      <MainContainer>
        <View style={{backgroundColor: '#ffffff', padding:10}}>
          <Text style={{fontSize:16}}>
            Name: { name }
          </Text>
          <Text style={{fontSize:16}}>
            Host: { host }
          </Text>
          <Text style={{fontSize:16}}>
            Port: { port }
          </Text>
          <Text style={{fontSize:16}}>
            Ip: { this.getAddress(addresses) }
          </Text>
        </View>
      </MainContainer>
    )
  }
}
