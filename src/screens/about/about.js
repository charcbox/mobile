import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { createStore } from 'redux';

import MainContainer from '../partials/mainContainer';

export default class About extends Component {
  render() {
    return (
      <MainContainer header="small" title="About">
        <Text>The About Page & mich more!</Text>
      </MainContainer>
    )
  }
}
