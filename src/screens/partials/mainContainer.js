import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

var styles = StyleSheet.create({
    mainContainer: {
    height: '100%',
    backgroundColor:'#f3f3f3ff',
    padding:20
  }
});

export default class MainContainer extends Component {
  constructor(props) {
    super(props)
  }

  render(){

    let showHeader = null
    if (this.props.header){
      showHeader = true
    }else{
      showHeader = false
    }

    return(
      <View style={{flex:1}}>
        {showHeader ?
            <Header title={this.props.title} type={this.props.header}>
              <View style={styles.mainContainer}>
                {this.props.children}
              </View>
            </Header>
          :
            <View style={styles.mainContainer}>
              {this.props.children}
            </View>
        }
      </View>
    )
  }
}

class Header extends Component {
  constructor(props) {
    super(props)
  }

  viewStyle(){
    if (this.props.type == "small"){
      return({
        alignItems: 'center',
        backgroundColor: '#e06666ff',
        height: 65})
    }else{
      return({
        alignItems: 'center',
        backgroundColor: '#e06666ff',
        height: 100})
    }
  }

  textStyle(){
    if (this.props.type == "small"){
      return({
        paddingTop: 30,
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        color: 'white',
        fontWeight: '600',
        fontSize: 22})
    }else{
      return({
        paddingTop: 30,
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 40})
    }
  }

  render(){
    return (
      <View  style={{ flex: 1 }}>
        <View style={this.viewStyle()}>
          <Text style={this.textStyle()}>
            {this.props.title}
          </Text>
        </View>
        <View style={{ flex: 1 }}>
          {this.props.children}
        </View>
      </View>
    )
  }
}
