import React, { Component } from 'react';
import {
  NetInfo
} from 'react-native'

import DeviceContainer from '../device/deviceContainer'
import MainContainer from '../partials/mainContainer'

//Device Store
var DeviceStore = require('../../config/stores/deviceStore')

// ZeroConf
import Zeroconf from 'react-native-zeroconf'
const CharcBoxName = "CharcBox"
const zeroconf = new Zeroconf()

// ToDo: Let's find a way to test this on local (SSID error)
// and on the real device (SSID CharcBox-Setup)
const charcBoxSetupSSID = "CharcBox-Setup"

export default class LaunchContainer extends Component {
  constructor(props){
    super(props)
    this.state = {
      connection: 'unknown',
      devices: DeviceStore.getDevices(),
    }
    this.getNetworkStatus = this.getNetworkStatus.bind(this)
    this.updateServices = this.updateServices.bind(this)
    this.errorServices = this.errorServices.bind(this)
    this.isOnWifi = this.isOnWifi.bind(this)
    this.getDevices = this.getDevices.bind(this)
    this.updateDevice = this.updateDevice.bind(this)
    this.findCharcboxIps = this.findCharcboxIps.bind(this)
  }

  componentWillMount() {
    zeroconf.scan()
    zeroconf.on('update', this.updateServices)
    zeroconf.on('error', this.errorServices)
    DeviceStore.subscribe(this.updateDevice)
    NetInfo.addEventListener('change', this.getNetworkStatus)
  }

  componentWillUnmount() {
    DeviceStore.unsubscribe(this.updateDevice);
    zeroconf.stop()
    NetInfo.removeEventListener('change', this.getNetworkStatus)
  }

  //listen to the store
  updateDevice() {
    this.setState({
      devices: DeviceStore.getDevices()
    })
  }

  //Needed if not will crash the App
  errorServices(){
    console.debug("errorService")
  }

  updateServices() {
    let services = zeroconf.getServices()
    ip_founds = this.findCharcboxIps(Object.values(services))
    DeviceStore.setDevices(ip_founds)
  }

  findCharcboxIps(services){
    ip_founds = []
    let regexp = new RegExp(CharcBoxName, 'gi');
    services.forEach(function(service) {
      if (service.name.match(regexp) && service.addresses && service.addresses.length > 0){
        ip_founds.push(service.addresses[0])
      }
    })
    return(ip_founds)
  }

  getNetworkStatus(){
    NetInfo.fetch().done((reach) => {
      this.setState({connection: reach})
    })
  }

  isOnWifi(){
    return(this.state.connection == "wifi")
  }

  getDevices(){
    return(this.state.devices)
  }

  render(){
    return(
      <DeviceContainer
        isOnWifi={this.isOnWifi}
        devices={this.getDevices}
      />
    )
  }

}
